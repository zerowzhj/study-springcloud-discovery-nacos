package test.study.springcloud.discovery.nacos;

import com.alibaba.cloud.nacos.registry.NacosRegistration;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import study.springcloud.discovery.nacos.support.SpringBootCfg;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootCfg.class)
public class RegistrationTest {

    @Autowired
    private NacosRegistration nacosRegistration;

    @Test
    public void test() {
        log.info(">>>>>>");
        log.info(">>>>>> {}", nacosRegistration.getServiceId());
        log.info(">>>>>> {}", nacosRegistration.getHost());
        log.info(">>>>>> {}", nacosRegistration.getNacosDiscoveryProperties().getHeartBeatInterval());
        log.info(">>>>>>");
    }
}
