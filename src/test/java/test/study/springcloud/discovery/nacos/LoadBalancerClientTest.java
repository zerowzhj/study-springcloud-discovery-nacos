package test.study.springcloud.discovery.nacos;

import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import study.eggs.result.Result;
import study.eggs.result.Results;
import study.springcloud.discovery.nacos.support.SpringBootCfg;

import java.util.Map;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootCfg.class)
public class LoadBalancerClientTest {

    @Autowired
    private LoadBalancerClient loadBalancerClient;

    @Test
    public void test() {
        ServiceInstance instance = loadBalancerClient.choose("");
        Map<String, Object> data = Maps.newHashMap();
        data.put("instanceId", instance.getInstanceId());
        data.put("scheme", instance.getScheme());
        data.put("host", instance.getHost());
        data.put("port", instance.getPort());
        data.put("uri", instance.getUri());
    }
}
