package test.study.springcloud.discovery.nacos;

import com.alibaba.nacos.api.naming.NamingFactory;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import study.springcloud.discovery.nacos.support.SpringBootCfg;

import java.util.List;
import java.util.Map;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootCfg.class)
public class NamingServiceTest {

    @Value("${spring.cloud.nacos.server-addr:}")
    private String serverAddr;

    @Test
    public void test() throws Exception {
        NamingService naming = NamingFactory.createNamingService(serverAddr);
        List<Instance> instanceLt = naming.getAllInstances("serviceId");
        List<Map<String, Object>> dataLt = Lists.newArrayList();
        for (Instance instance : instanceLt) {
            Map<String, Object> data = Maps.newHashMap();
            data.put("instanceId", instance.getInstanceId());
            data.put("serviceName", instance.getServiceName());
            data.put("clusterName", instance.getClusterName());
            data.put("ip", instance.getIp());
            data.put("port", instance.getPort());
            dataLt.add(data);
        }
    }
}
