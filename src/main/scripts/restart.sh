#!/bin/bash

DEPLOY_DIR=/xdfapp/study-springcloud-discovery-nacos
JAR_NAME=study-springcloud-discovery-nacos-1.0.jar

pid=$(ps -ef | grep $JAR_NAME | grep -v grep | awk '{print $2}')
echo "[INFO] pid= [$pid]"
if [ ! -n "$pid" ]; then
  echo "[WARN] No Service Running"
else
  echo "[INFO] Stop Service: $pid"
  sudo kill -9 $pid
  sleep 2
fi

source /etc/profile
nohup java -jar $DEPLOY_DIR/$JAR_NAME >/dev/null 2>&1 &
#nohup java -jar $DEPLOY_DIR/$JAR_NAME &
echo "[INFO] Service Started"
exit
