package study.springcloud.discovery.nacos.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import study.eggs.result.Result;
import study.eggs.result.Results;


@Slf4j
@RestController
public class ClientController {

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/client/echo")
    public Result client() {
        String url = "http://study-springcloud-discovery-nacos/echo?name=wangzhj";
        String body = restTemplate.getForObject(url, String.class);
        log.info(">>>>>> {}", body);
        return Results.ok();
    }

    @RequestMapping("/echo")
    public Result echo(String name) {
        log.info("echo echo echo, {}", name);
        return Results.ok();
    }
}
