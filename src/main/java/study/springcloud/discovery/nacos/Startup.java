package study.springcloud.discovery.nacos;

import org.springframework.boot.SpringApplication;
import study.springcloud.discovery.nacos.support.SpringBootCfg;

public class Startup {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootCfg.class, args);
    }
}
