package study.springcloud.discovery.nacos.support.nacos;

import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.alibaba.cloud.nacos.registry.NacosRegistrationCustomizer;
import com.alibaba.cloud.nacos.registry.NacosServiceRegistryAutoConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Slf4j
@Configuration
@AutoConfigureBefore(NacosServiceRegistryAutoConfiguration.class)
public class MyNacosServiceRegistryAutoConfiguration {

//    @Primary
//    @Bean
    public MyNacosServiceRegistry serviceRegistry(NacosDiscoveryProperties nacosDiscoveryProperties) {
        return new MyNacosServiceRegistry(nacosDiscoveryProperties);
    }

//    @Bean
    public NacosRegistrationCustomizer customizer() {
        return new MyNacosRegistrationCustomizer();
    }
}
