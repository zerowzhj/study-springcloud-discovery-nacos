package study.springcloud.discovery.nacos.support.nacos;

import com.alibaba.cloud.nacos.registry.NacosRegistration;
import com.alibaba.cloud.nacos.registry.NacosRegistrationCustomizer;

public class MyNacosRegistrationCustomizer implements NacosRegistrationCustomizer {

    @Override
    public void customize(NacosRegistration registration) {
        registration.getNacosDiscoveryProperties().setService("fffffffffff");
    }
}
