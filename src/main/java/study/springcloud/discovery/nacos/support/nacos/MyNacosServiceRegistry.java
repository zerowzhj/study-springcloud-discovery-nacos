package study.springcloud.discovery.nacos.support.nacos;

import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.alibaba.cloud.nacos.registry.NacosRegistration;
import com.alibaba.cloud.nacos.registry.NacosServiceRegistry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.serviceregistry.Registration;

@Slf4j
public class MyNacosServiceRegistry extends NacosServiceRegistry {

    public MyNacosServiceRegistry(NacosDiscoveryProperties nacosDiscoveryProperties) {
        super(nacosDiscoveryProperties);
    }

    @Override
    public void register(Registration registration) {
        log.info("sssssssssss");
        NacosRegistration myRegistration = (NacosRegistration) registration;
        NacosDiscoveryProperties pro =  myRegistration.getNacosDiscoveryProperties();
        pro.setService("sssssssssssssssssss");
        super.register(registration);
    }
}
